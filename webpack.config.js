const path = require('path');

const hotel = process.env.HOTEL || 'no-hotel';

module.exports = {
    mode: "production",
    entry: './src/index.js',
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'dist', hotel),
    },
};